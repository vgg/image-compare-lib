# OpenCV Emscripten build without pthreads

You can download the pre-built binaries from [`vgg/packages`](https://gitlab.com/vgg/packages/-/packages/8351401) package registry.

You can also build your own version if you prefer.

Check out the [repo](https://gitlab.com/vgg/packages) for documentation and script to re-build.