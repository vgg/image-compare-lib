cmake_minimum_required(VERSION 3.16)
project(transform)

include(CMakeDependentOption)

CMAKE_DEPENDENT_OPTION(BUILD_DEMO "Build demo app" OFF "NOT BUILD_BINDINGS" OFF)
CMAKE_DEPENDENT_OPTION(BUILD_BINDINGS "Build JS bindings" OFF "NOT BUILD_DEMO" OFF)

# Find OpenCV
find_package(OpenCV 4.0 REQUIRED COMPONENTS core imgproc features2d calib3d shape)

# Find Eigen
# https://eigen.tuxfamily.org/dox/TopicCMakeGuide.html
find_package(Eigen3 3.0 REQUIRED NO_MODULE)

# Build library
add_library(
    transform_lib
    src/transform.hpp
    src/transform.cpp
)
target_include_directories(transform_lib PUBLIC src)
set_target_properties(transform_lib PROPERTIES CXX_STANDARD 11 CXX_STANDARD_REQUIRED YES)
target_link_libraries(transform_lib opencv_core opencv_imgproc opencv_features2d opencv_calib3d opencv_shape Eigen3::Eigen)

if(BUILD_DEMO)
    find_package(OpenCV 4.0 REQUIRED COMPONENTS imgcodecs highgui)
    add_executable(demo demo/main.cpp)
    target_link_libraries(demo transform_lib opencv_imgcodecs opencv_highgui)
    set_target_properties(demo PROPERTIES CXX_STANDARD 11 CXX_STANDARD_REQUIRED YES)
endif(BUILD_DEMO)

if(BUILD_BINDINGS)
    # # Find Python
    find_package(Python REQUIRED COMPONENTS Interpreter)

    # # Find Emscripten
    find_path(
        EMSCRIPTEN_INCLUDE_DIR
        emscripten/bind.h
        PATHS
        ENV EMSCRIPTEN_ROOT
        PATH_SUFFIXES system/include include
        DOC "Location of Emscripten SDK")

    # Add target for transform_js
    add_executable(
        transform_js
        src/bindings.cpp
    )
    target_link_libraries(transform_js transform_lib)
    target_include_directories(
        transform_js
        PRIVATE
        src/
        ${EMSCRIPTEN_INCLUDE_DIR}
    )
    set_target_properties(
        transform_js
        PROPERTIES
        CXX_STANDARD 11
        CXX_STANDARD_REQUIRED YES
    )

    set(EMSCRIPTEN_LINK_FLAGS "${EMSCRIPTEN_LINK_FLAGS} -s USE_PTHREADS=0 --memory-init-file 0 -s TOTAL_MEMORY=128MB -s WASM_MEM_MAX=1GB -s ALLOW_MEMORY_GROWTH=1")
    set(EMSCRIPTEN_LINK_FLAGS "${EMSCRIPTEN_LINK_FLAGS} -s MODULARIZE=1 -s SINGLE_FILE=1")
    set(EMSCRIPTEN_LINK_FLAGS "${EMSCRIPTEN_LINK_FLAGS} -s EXPORT_NAME=\"'transform'\" -s DEMANGLE_SUPPORT=1")

    set(EMSCRIPTEN_LINK_FLAGS "${EMSCRIPTEN_LINK_FLAGS} --bind -O3")

    # set(EMSCRIPTEN_LINK_FLAGS "${EMSCRIPTEN_LINK_FLAGS} --bind -g4 -gsource-map -s ASSERTIONS=1")
    set_target_properties(transform_js PROPERTIES LINK_FLAGS "${EMSCRIPTEN_LINK_FLAGS}")

    # add UMD wrapper
    set(MODULE_JS_PATH "${CMAKE_BINARY_DIR}/transform_js.js")
    set(JS_PATH "${CMAKE_BINARY_DIR}/transform.js")

    add_custom_command(
        OUTPUT transform.js
        COMMAND ${Python_EXECUTABLE} "${CMAKE_CURRENT_SOURCE_DIR}/src/make_umd.py" ${MODULE_JS_PATH} "${JS_PATH}"
        DEPENDS transform_js
        DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/src/make_umd.py")

    add_custom_target(transform ALL
        DEPENDS ${JS_PATH}
        DEPENDS transform_js)
endif(BUILD_BINDINGS)