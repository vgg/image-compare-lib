#ifndef __TRANSFORM_HPP__
#define __TRANSFORM_HPP__

#include <opencv2/core/mat.hpp>
#include <string>

namespace transform
{
    struct MeanStddev
    {
        double mean;
        double stddev;
    };
    cv::Mat estimate_transform(
        const cv::Mat &query,
        const cv::Mat &target,
        const std::string &_transform = "affine",
        const double _scale_query = -1.0,
        const double _scale_target = -1.0);

    cv::Mat warp_image(
        const cv::Mat &query,
        const cv::Mat &H,
        const cv::Size &target_size,
        const std::string &_transform = "affine");

    MeanStddev get_luma_mean_stddev(const cv::Mat &im);
}
#endif