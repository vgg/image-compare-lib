#include "transform.hpp"

#include <Eigen/Dense>
#include <opencv2/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/flann.hpp>
#include <opencv2/calib3d.hpp>

#include <vector>
#include <random>
#include <string>

using namespace Eigen;
const int MAX_DIM = 1024;

double get_scaling_factor_for_resize(const cv::Mat &im, const int _dim = MAX_DIM)
{
    auto r = im.rows;
    auto c = im.cols;

    if (r <= _dim && c <= _dim)
    {
        return 1.0;
    }

    auto max_dim = std::max(r, c);
    auto scale = 1.0 * _dim / max_dim;

    return scale;
}

// Get normalization matrix
void get_norm_matrix(const MatrixXd &pts, Matrix<double, 3, 3> &T)
{
    Vector3d mu = pts.rowwise().mean();
    MatrixXd norm_pts = (pts).colwise() - mu;

    VectorXd x2 = norm_pts.row(0).array().pow(2);
    VectorXd y2 = norm_pts.row(1).array().pow(2);
    VectorXd dist = (x2 + y2).array().sqrt();
    double scale = sqrt(2) / dist.array().mean();

    T(0, 0) = scale;
    T(0, 1) = 0;
    T(0, 2) = -scale * mu(0);
    T(1, 0) = 0;
    T(1, 1) = scale;
    T(1, 2) = -scale * mu(1);
    T(2, 0) = 0;
    T(2, 1) = 0;
    T(2, 2) = 1;
}

// Implementation of Direct Linear Transform (DLT) algorithm as described in pg. 91
// Multiple View Geometry in Computer Vision, Richard Hartley and Andrew Zisserman, 2nd Edition
// assumption: X and Y are point correspondences. Four 2D points. (4 X 3 in homogenous coordinates)
// X and Y : n x 3 matrix
// H : 3x3 matrix
void dlt(const MatrixXd &X, const MatrixXd &Y, Matrix<double, 3, 3> &H)
{
    size_t n = X.rows();

    MatrixXd A(2 * n, 9);
    A.setZero();
    for (size_t i = 0; i < n; ++i)
    {
        A.row(2 * i).block(0, 3, 1, 3) = -Y(i, 2) * X.row(i);
        A.row(2 * i).block(0, 6, 1, 3) = Y(i, 1) * X.row(i);

        A.row(2 * i + 1).block(0, 0, 1, 3) = Y(i, 2) * X.row(i);
        A.row(2 * i + 1).block(0, 6, 1, 3) = -Y(i, 0) * X.row(i);
    }

    JacobiSVD<MatrixXd> svd(A, ComputeFullV);
    // Caution: the last column of V are reshaped into 3x3 matrix as shown in Pg. 89
    // of Hartley and Zisserman (2nd Edition)
    // svd.matrixV().col(8).array().resize(3,3) is not correct!
    H << svd.matrixV()(0, 8), svd.matrixV()(1, 8), svd.matrixV()(2, 8),
        svd.matrixV()(3, 8), svd.matrixV()(4, 8), svd.matrixV()(5, 8),
        0, 0, svd.matrixV()(8, 8); // affine transform
                                   // svd.matrixV()(6,8), svd.matrixV()(7,8), svd.matrixV()(8,8); // projective transform
}

void estimate_transform(const MatrixXd &X, const MatrixXd &Y, const std::string &transform, Matrix<double, 3, 3> &T)
{
    if (transform == "identity")
    {
        T << 1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0;
    }

    if (transform == "translation" || transform == "rigid" || transform == "similarity")
    {
        // ignore homogenous representation
        Matrix<double, 4, 2> X_, Y_;
        X_ = X.block<4, 2>(0, 0);
        Y_ = Y.block<4, 2>(0, 0);

        // example values to test with spot the difference demo image.
        // should give identity rotation
        // X_ <<  -1.1684, -1.34154,
        //         -1.3568, -1.28858,
        //         -1.08083,-1.0768,
        //         -1.47721,-1.02969;
        // Y_  << -1.15706, -1.33154,
        //         -1.35822,-1.28466,
        //         -1.08694, -1.07509,
        //         -1.48525, -1.02964;

        size_t m = X_.cols();
        size_t n = X_.rows();

        Matrix<double, 2, 4> Xt_, Yt_;
        Xt_ = X_.transpose();
        Yt_ = Y_.transpose();

        // subtract mean from points
        VectorXd X_mu = Xt_.rowwise().mean();
        VectorXd Y_mu = Yt_.rowwise().mean();

        Xt_.colwise() -= X_mu;
        Yt_.colwise() -= Y_mu;
        MatrixXd X_norm = Xt_.transpose();
        MatrixXd Y_norm = Yt_.transpose();

        // compute matrix A first (3 X 3 rotation matrix)
        MatrixXd A = (1.0 / n) * (Y_norm.transpose() * X_norm);
        // rotation matrix to compute
        Matrix<double, 2, 2> R;
        Matrix<double, 2, 2> d = Matrix<double, 2, 2>::Identity();

        // do SVD of A
        JacobiSVD<MatrixXd> svd_u(A, ComputeFullU);
        Matrix<double, 2, 2> U = svd_u.matrixU();
        U(0, 0) = -1.0 * U(0, 0);
        U(1, 0) = -1.0 * U(1, 0);
        JacobiSVD<MatrixXd> svd_v(A, ComputeFullV);
        Matrix<double, 2, 2> V = svd_v.matrixV();
        V(0, 0) = -1.0 * V(0, 0);
        V(1, 0) = -1.0 * V(1, 0);

        if (svd_u.rank() == 0)
        {
            std::cout << "SVD leads to 0 rank!" << std::endl;
            return;
        }

        if (svd_u.rank() == (m - 1))
        {
            if ((U.determinant() * V.determinant()) > 0)
            {
                R = U * V;
            }
            else
            {
                double s = d(1, 1);
                d(1, 1) = -1;
                R = U * d * V;
                d(1, 1) = s;
            }
        }
        else
        {
            R = U * d * V;
        }

        double scale = 1.0;
        // compute scale if its similarity transform
        if (transform == "similarity")
        {
            double s_dot_d = svd_u.singularValues().dot(d.diagonal());
            double x_norm_var = X_norm.transpose().cwiseAbs2().rowwise().mean().sum();
            scale = (1.0 / x_norm_var) * (svd_u.singularValues().dot(d.diagonal()));
        }
        // apply scale to rotation and translation
        Vector2d t = Y_mu - (scale * (R * X_mu));
        R = R * scale;

        if (transform == "translation")
        {
            // apply identity rotation
            T << 1, 0, t(0),
                0, 1, t(1),
                0, 0, 1;
        }
        else
        {
            T << R(0, 0), R(0, 1), t(0),
                R(1, 0), R(1, 1), t(1),
                0, 0, 1.0;
        }
    }

    if (transform == "affine")
    {
        // estimate affine transform using DLT algorithm
        dlt(X, Y, T);
    }
    // std::cout << "Final T value is: " << std::endl;
    // std::cout << T << std::endl;
}

Eigen::MatrixXd _estimate_tps_W(const Eigen::MatrixXd &H, const Eigen::MatrixXd &H2, const unsigned int num_pts)
{
    double lambda = 0.001;
    double lambda_norm = lambda * (num_pts);
    const auto n_cp = H.cols();
    // create matrix K
    MatrixXd K(n_cp, n_cp);
    K.setZero();
    double rx, ry, r, r2;
    for (unsigned int i = 0; i < n_cp; ++i)
    {
        for (unsigned int j = i; j < n_cp; ++j)
        {
            // image grid coordinate = (i,j)
            // control point coordinate = cp(:,k)
            rx = H(0, i) - H(0, j);
            ry = H(1, i) - H(1, j);
            r = sqrt(rx * rx + ry * ry);
            r2 = r * r;
            if (r > 0)
            {
                K(i, j) = r2 * log(r);
            }
            else
            {
                K(i, j) = 0;
            }
            K(j, i) = K(i, j);
        }
    }

    MatrixXd P(n_cp, 3);
    for (unsigned int i = 0; i < n_cp; ++i)
    {
        // K(i,i) = 0; // ensure that the diagonal elements of K are 0 (as log(0) = nan)
        //  approximating thin-plate splines based on:
        //  Rohr, K., Stiehl, H.S., Sprengel, R., Buzug, T.M., Weese, J. and Kuhn, M.H., 2001. Landmark-based elastic registration using approximating thin-plate splines.
        K(i, i) = ((double)n_cp) * lambda * 1;
        P(i, 0) = 1;
        P(i, 1) = H(0, i);
        P(i, 2) = H(1, i);
    }

    // create matrix L
    MatrixXd L(n_cp + 3, n_cp + 3);
    L.block(0, 0, n_cp, n_cp) = K;
    L.block(0, n_cp, n_cp, 3) = P;
    L.block(n_cp, 0, 3, n_cp) = P.transpose();
    L.block(n_cp, n_cp, 3, 3).setZero();

    // create matrix V
    MatrixXd V(n_cp + 3, 2);
    V.setZero();
    for (unsigned int i = 0; i < n_cp; ++i)
    {
        V(i, 0) = H2(0, i);
        V(i, 1) = H2(1, i);
    }

    MatrixXd Linv = L.inverse();
    MatrixXd W = Linv * V;

    return W;
}

std::pair<std::vector<cv::Point2f>, std::vector<cv::Point2f>> find_matches(
    const cv::Mat &query,
    const cv::Mat &target)
{
    // Containers for keypoints and descriptors
    std::vector<cv::KeyPoint> kp_query, kp_target;
    cv::Mat descriptor_query, descriptor_target;

    // Find features in both the images
    auto detector_query = cv::SIFT::create();
    auto detector_target = cv::SIFT::create();
    detector_query->detectAndCompute(query, cv::noArray(), kp_query, descriptor_query);
    detector_target->detectAndCompute(target, cv::noArray(), kp_target, descriptor_target);

    auto matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
    std::vector<std::vector<cv::DMatch>> matches;

    matcher->knnMatch(descriptor_query, descriptor_target, matches, 2);

    //-- Filter matches using the Lowe's ratio test
    // TODO parameter value
    const float ratio_thresh = 0.75f;
    std::vector<std::pair<cv::DMatch, double>> good_matches_with_ratio;
    for (size_t i = 0; i < matches.size(); i++)
    {
        const double ratio = matches[i][0].distance / matches[i][1].distance;
        if (ratio < ratio_thresh)
        {
            good_matches_with_ratio.push_back(std::make_pair(matches[i][0], ratio));
        }
    }

    std::vector<cv::Point2f> query_pts;
    std::vector<cv::Point2f> target_pts;

    if (good_matches_with_ratio.size() < 9)
    {
        return std::make_pair(query_pts, target_pts);
    }

    std::sort(
        good_matches_with_ratio.begin(),
        good_matches_with_ratio.end(),
        [](
            const std::pair<cv::DMatch, double> &i,
            const std::pair<cv::DMatch, double> &j) -> bool
        {
            return i.second < j.second;
        });

    for (size_t i = 0; i < good_matches_with_ratio.size(); i++)
    {
        //-- Get the keypoints from the good matches
        query_pts.push_back(kp_query[good_matches_with_ratio[i].first.queryIdx].pt);
        target_pts.push_back(kp_target[good_matches_with_ratio[i].first.trainIdx].pt);
    }
    return std::make_pair(query_pts, target_pts);
}

cv::Mat find_tps_transform(
    const Eigen::MatrixXd &pts1,
    const Eigen::MatrixXd &pts2,
    cv::Size im1_size)
{
    cv::Mat cvW;

    const unsigned int n_lowe_match = pts1.cols();
    Matrix3d pts1_tform, pts2_tform;
    get_norm_matrix(pts1, pts1_tform);
    get_norm_matrix(pts2, pts2_tform);
    // std::cout << "Normalizing matrix (T) = " << pts1_tform << std::endl;

    MatrixXd pts1_norm = pts1_tform * pts1;
    MatrixXd pts2_norm = pts2_tform * pts2;

    // form a matrix containing match pair (x,y) <-> (x',y') as follows
    // [x y x' y'; ...]
    MatrixXd S_all(4, n_lowe_match);
    S_all.row(0) = pts1_norm.row(0);
    S_all.row(1) = pts1_norm.row(1);
    S_all.row(2) = pts2_norm.row(0);
    S_all.row(3) = pts2_norm.row(1);

    // initialize random number generator to randomly sample putative_matches
    std::mt19937 generator(9973);
    std::uniform_int_distribution<> dist(0, n_lowe_match - 1);

    MatrixXd S(4, 3), hatS(4, 3);
    double residual;

    std::vector<size_t> best_robust_match_idx;
    // @todo: tune this threshold for better generalization
    double robust_ransac_threshold = 0.09;
    // double robust_ransac_threshold = 0.2;

    double inlier_fraction = 0.1;
    double outlier_fraction = 1 - inlier_fraction;

    const unsigned int n_sample = 3;
    double p = 0.995;
    double log_1_minus_p = log(1 - p);
    auto lambda_N = [&]() -> size_t
    { return (size_t)round(log_1_minus_p / (log(1 - std::pow(inlier_fraction, n_sample))) - 1e-5); };

    size_t RANSAC_ITER_COUNT = lambda_N();
    printf("Ransac iteration count: %zu\n", RANSAC_ITER_COUNT);
    // cout << "RANSAC_ITER_COUNT = " << RANSAC_ITER_COUNT << endl;
    for (int i = 0; i < RANSAC_ITER_COUNT; ++i)
    {
        S.col(0) = S_all.col(dist(generator)); // randomly select a match from S_all
        S.col(1) = S_all.col(dist(generator));
        S.col(2) = S_all.col(dist(generator));

        Vector4d muS = S.rowwise().mean();
        hatS = S.colwise() - muS;

        JacobiSVD<MatrixXd> svd(hatS, ComputeFullU);

        MatrixXd As = svd.matrixU().block(0, 0, svd.matrixU().rows(), 2);

        MatrixXd dx = S_all.colwise() - muS;
        MatrixXd del = dx - (As * As.transpose() * dx);

        std::vector<size_t> robust_match_idx;
        robust_match_idx.clear();
        for (int j = 0; j < del.cols(); ++j)
        {
            residual = sqrt(del(0, j) * del(0, j) + del(1, j) * del(1, j) + del(2, j) * del(2, j) + del(3, j) * del(3, j));
            if (residual < robust_ransac_threshold)
            {
                robust_match_idx.push_back(j);
            }
        }
        // cout << i << ": robust_match_idx=" << robust_match_idx.size() << endl;
        // printf("Robust match size: %zu\n", robust_match_idx.size());
        if (robust_match_idx.size() > best_robust_match_idx.size())
        {
            printf("Best Robust match size: %zu\n", robust_match_idx.size());
            best_robust_match_idx.clear();
            best_robust_match_idx.swap(robust_match_idx);

            // Update iteration count
            inlier_fraction = best_robust_match_idx.size() / (double)n_lowe_match;
            printf("Inlier fraction %lf\n", inlier_fraction);
            RANSAC_ITER_COUNT = lambda_N();
            if (RANSAC_ITER_COUNT == 0 || RANSAC_ITER_COUNT > 1e4)
            {
                RANSAC_ITER_COUNT = 1e4;
            }
            printf("Ransac iteration count: %zu, inlier_fraction %lf\n", RANSAC_ITER_COUNT, inlier_fraction);
            // cout << "[MIN]" << endl;
        }
    } // end RANSAC_ITER_COUNT loop

    if (best_robust_match_idx.size() < 3)
    {
        return cvW;
    }
    // cout << "robust match pairs = " << fp_match_count << endl;

    // bin each correspondence pair into cells dividing the original image into KxK cells
    // a single point in each cell ensures that no two control points are very close
    unsigned int POINTS_PER_CELL = 1;
    unsigned int n_cell_w, n_cell_h;

    if (im1_size.height > 500)
    {
        n_cell_h = 16;
    }
    else
    {
        n_cell_h = 8;
    }
    unsigned int ch = (unsigned int)(im1_size.height / n_cell_h);

    if (im1_size.width > 500)
    {
        n_cell_w = 16;
    }
    else
    {
        n_cell_w = 8;
    }
    unsigned int cw = (unsigned int)(im1_size.width / n_cell_w);
    // printf("n_cell_w=%d, n_cell_h=%d, cw=%d, ch=%d", n_cell_w, n_cell_h, cw, ch);
    // printf("image size = %ld x %ld", im1.columns(), im1.rows());
    // TODO: Rewrite loop to assign points to cells rather than other way around.
    std::vector<size_t> sel_best_robust_match_idx;
    for (unsigned int i = 0; i < n_cell_w; ++i)
    {
        for (unsigned int j = 0; j < n_cell_h; ++j)
        {
            unsigned int xl = i * cw;
            unsigned int xh = (i + 1) * cw - 1;
            if (xh > im1_size.width)
            {
                xh = im1_size.width - 1;
            }

            unsigned int yl = j * ch;
            unsigned int yh = (j + 1) * ch - 1;
            if (yh > im1_size.height)
            {
                yh = im1_size.height - 1;
            }

            // printf("\ncell(%d,%d) = (%d,%d) to (%d,%d)\n", i, j, xl, yl, xh, yh);
            // cout << flush;

            std::vector<size_t> cell_pts;
            for (unsigned int k = 0; k < best_robust_match_idx.size(); ++k)
            {
                size_t match_idx = best_robust_match_idx.at(k);
                double x = pts1(0, match_idx);
                double y = pts1(1, match_idx);

                if (x >= xl && x < xh && y >= yl && y < yh)
                {
                    cell_pts.push_back(match_idx);
                }
            }
            // printf("\ncell size: %d\n", cell_pts.size());
            if (cell_pts.size() >= POINTS_PER_CELL)
            {
                sel_best_robust_match_idx.push_back(cell_pts.at(0));

                // std::uniform_int_distribution<> dist2(0, cell_pts.size() - 1);
                // for (unsigned int k = 0; k < POINTS_PER_CELL; ++k)
                // {
                //     unsigned long cell_pts_idx = dist2(generator);
                //     sel_best_robust_match_idx.push_back(cell_pts.at(cell_pts_idx));
                // }
            }
        }
    }
    size_t n_cp = sel_best_robust_match_idx.size();
    // size_t n_cp = best_robust_match_idx.size();
    Eigen::MatrixXd cp1, cp2;
    cp1.resize(2, n_cp);
    cp2.resize(2, n_cp);

    for (size_t i = 0; i < n_cp; ++i)
    {
        unsigned long match_idx = sel_best_robust_match_idx.at(i);
        // auto match_idx = best_robust_match_idx.at(i);
        cp1(0, i) = pts1(0, match_idx);
        cp1(1, i) = pts1(1, match_idx);
        cp2(0, i) = pts2(0, match_idx);
        cp2(1, i) = pts2(1, match_idx);
    }

    auto W = _estimate_tps_W(cp1, cp2, im1_size.width * im1_size.height);
    // Convert W to cvW
    cv::eigen2cv(W, cvW);

    if (cvW.empty())
    {
        return cvW;
    }
    // Convert control points to cv mat first
    cv::Mat cvH;
    cv::eigen2cv(cp1, cvH);

    // cvH is 2 x n_cp matrix, convert to n_cp+3 x 2
    cv::Mat H_expanded = cv::Mat::zeros(cvH.cols + 3, cvH.rows, cvH.type());

    //  Transpose, copy and stack
    cv::Mat output;
    cv::Mat cvH_t = cvH.t();
    cvH_t.copyTo(H_expanded.rowRange(0, cvH.cols));
    cv::hconcat(cvW, H_expanded, output);
    return output;
}

cv::Mat find_dlt_transform(
    const Eigen::MatrixXd &im1_match_kp,
    const Eigen::MatrixXd &im2_match_kp,
    const std::string &_transform)
{
    cv::Mat cvH;

    const int n_match = im1_match_kp.cols();
    Matrix<double, 3, 3> im1_match_kp_tform, im2_match_kp_tform;
    get_norm_matrix(im1_match_kp, im1_match_kp_tform);
    get_norm_matrix(im2_match_kp, im2_match_kp_tform);
    MatrixXd im2_match_kp_tform_inv = im2_match_kp_tform.inverse();

    MatrixXd im1_match_norm = im1_match_kp_tform * im1_match_kp;
    MatrixXd im2_match_norm = im2_match_kp_tform * im2_match_kp;

    // initialize random number generator to randomly sample putative_matches
    std::random_device rand_device;
    std::mt19937 generator(rand_device());
    std::uniform_int_distribution<> dist(0, n_match - 1);

    // estimate homography using RANSAC
    size_t max_score = 0;
    Matrix<double, 3, 3> Hi;
    std::vector<unsigned int> best_inliers_index;

    // see Hartley and Zisserman p.119
    // in the original image 2 domain, error = sqrt(5.99) * 1 ~ 3 (for SD of 1 pixel error)
    // in the normalized image 2 domain, we have to transform this 3 pixel to normalized coordinates
    double geom_err_threshold_norm = im2_match_kp_tform(0, 0) * 3;

    double inlier_fraction = 0.1;
    double outlier_fraction = 1 - inlier_fraction;

    const unsigned int n_sample = 3;
    double p = 0.995;
    double log_1_minus_p = log(1 - p);
    auto lambda_N = [&]() -> size_t
    { return (size_t)round(log_1_minus_p / (log(1 - std::pow(inlier_fraction, n_sample))) - 1e-5); };

    size_t RANSAC_ITER_COUNT = lambda_N();
    printf("Ransac iteration count: %zu\n", RANSAC_ITER_COUNT);

    for (unsigned int iter = 0; iter < RANSAC_ITER_COUNT; iter++)
    {
        // cout << "==========================[ iter=" << iter << " ]==============================" << endl;

        // randomly select 4 matches from putative_matches
        int kp_id1 = dist(generator);
        int kp_id2 = dist(generator);
        int kp_id3 = dist(generator);
        int kp_id4 = dist(generator);
        // cout << "Random entries from putative_matches: " << kp_id1 << "," << kp_id2 << "," << kp_id3 << "," << kp_id4 << endl;

        MatrixXd X(4, 3);
        X.row(0) = im1_match_norm.col(kp_id1).transpose();
        X.row(1) = im1_match_norm.col(kp_id2).transpose();
        X.row(2) = im1_match_norm.col(kp_id3).transpose();
        X.row(3) = im1_match_norm.col(kp_id4).transpose();

        MatrixXd Y(4, 3);
        Y.row(0) = im2_match_norm.col(kp_id1).transpose();
        Y.row(1) = im2_match_norm.col(kp_id2).transpose();
        Y.row(2) = im2_match_norm.col(kp_id3).transpose();
        Y.row(3) = im2_match_norm.col(kp_id4).transpose();

        // dlt(X, Y, Hi);
        estimate_transform(X, Y, _transform, Hi);

        size_t score = 0;
        std::vector<unsigned int> inliers_index;
        inliers_index.reserve(n_match);
        MatrixXd im1tx_norm = Hi * im1_match_norm; // 3 x n

        im1tx_norm.row(0) = im1tx_norm.row(0).array() / im1tx_norm.row(2).array();
        im1tx_norm.row(1) = im1tx_norm.row(1).array() / im1tx_norm.row(2).array();

        MatrixXd del(2, n_match);
        del.row(0) = im1tx_norm.row(0) - im2_match_norm.row(0);
        del.row(1) = im1tx_norm.row(1) - im2_match_norm.row(1);
        del = del.array().pow(2);
        VectorXd error = del.row(0) + del.row(1);
        error = error.array().sqrt();

        for (size_t k = 0; k < n_match; k++)
        {
            if (error(k) < geom_err_threshold_norm)
            {
                score++;
                inliers_index.push_back(k);
            }
        }
        // cout << "iter " << iter << " of " << RANSAC_ITER_COUNT << " : score=" << score << ", max_score=" << max_score << ", total_matches=" << putative_matches.size() << endl;
        if (score > max_score)
        {
            max_score = score;
            best_inliers_index.swap(inliers_index);

            // Update iteration count
            inlier_fraction = best_inliers_index.size() / (double)n_match;
            RANSAC_ITER_COUNT = lambda_N();
            if (RANSAC_ITER_COUNT == 0 || RANSAC_ITER_COUNT > 1e4)
            {
                RANSAC_ITER_COUNT = 1e4;
            }
            printf("Ransac iteration count: %zu, inlier_fraction %lf\n", RANSAC_ITER_COUNT, inlier_fraction);
        }
    } // end RANSAC_ITER_COUNT loop
    // high_resolution_clock::time_point after_ransac = std::chrono::high_resolution_clock::now();
    // cout << "after ransac is: " << (duration_cast<duration<double>>(after_ransac - after_putative_match)).count() << endl;

    if (_transform != "identity" && max_score < 3)
    {
        return cvH;
    }

    // Recompute homography using all the inliers
    // This does not improve the registration
    // cout << "re-computing homography using inliers" << endl;
    size_t n_inliers = best_inliers_index.size();
    MatrixXd X(n_inliers, 3);
    MatrixXd Y(n_inliers, 3);
    for (size_t i = 0; i < n_inliers; ++i)
    {
        X.row(i) = im1_match_norm.col(best_inliers_index.at(i)).transpose();
        Y.row(i) = im2_match_norm.col(best_inliers_index.at(i)).transpose();
    }

    Matrix<double, 3, 3> Hopt_norm;

    // dlt(X, Y, Hopt_norm);
    // cout << "estimateing transform: " << transform << endl;
    estimate_transform(X, Y, _transform, Hopt_norm);

    // see Hartley and Zisserman p.109
    Eigen::MatrixXd H;

    H = im2_match_kp_tform_inv * Hopt_norm * im1_match_kp_tform;
    H = H / H(2, 2);

    cv::eigen2cv(H, cvH);
    return cvH;
}

cv::Mat find_perspective_transform(
    const std::vector<cv::Point2f> &src_pts,
    const std::vector<cv::Point2f> &dst_pts)
{
    // normalize points?
    return cv::findHomography(src_pts, dst_pts, cv::RANSAC, 3.0);
}

cv::Mat find_transform(
    const std::vector<cv::Point2f> &src_pts,
    const std::vector<cv::Point2f> &dst_pts,
    const std::string &_transform,
    const cv::Size &im2_size)
{
    if (_transform == "perspective")
    {
        auto H = find_perspective_transform(src_pts, dst_pts);
        if (H.empty())
        {
            return H;
        }
        cv::Mat H_inv;
        cv::invert(H, H_inv);
        return H_inv;
    }

    const auto n_match = src_pts.size();
    Eigen::MatrixXd im1_match_kp(3, n_match);
    Eigen::MatrixXd im2_match_kp(3, n_match);

    for (size_t i = 0; i < n_match; i++)
    {
        im1_match_kp(0, i) = src_pts.at(i).x;
        im1_match_kp(1, i) = src_pts.at(i).y;
        im1_match_kp(2, i) = 1.0;

        im2_match_kp(0, i) = dst_pts.at(i).x;
        im2_match_kp(1, i) = dst_pts.at(i).y;
        im2_match_kp(2, i) = 1.0;
    }

    if (_transform == "tps")
    {
        // Find the map which will take query to target
        return find_tps_transform(im2_match_kp, im1_match_kp, im2_size);
    }
    else
    {
        auto H = find_dlt_transform(im1_match_kp, im2_match_kp, _transform);
        if (H.empty())
        {
            return H;
        }

        cv::Mat H_inv;
        cv::invert(H, H_inv);
        return H_inv;
    }
}

cv::Mat transform::estimate_transform(
    const cv::Mat &query,
    const cv::Mat &target,
    const std::string &_transform,
    const double _scale_query,
    const double _scale_target)
{
    // Resize image if needed and remember the scaling factor
    const auto scale_query = _scale_query == -1.0 ? get_scaling_factor_for_resize(query) : _scale_query;
    const auto scale_target = _scale_target == -1.0 ? get_scaling_factor_for_resize(target) : _scale_target;

    // Setup resized target
    cv::Mat resized_target;
    if (_scale_target == -1.0)
    {
        // Resize target image if needed
        if (scale_target < 1)
        {
            cv::resize(target, resized_target, cv::Size(), scale_target, scale_target);
        }
        else
        {
            target.copyTo(resized_target);
        }
    }
    else
    {
        // Assume input is already resized by caller
        target.copyTo(resized_target);
    }

    // Setup resized query
    cv::Mat resized_query;
    if (_scale_query == -1.0)
    {
        // Resize query image if needed
        if (scale_query < 1)
        {
            cv::resize(query, resized_query, cv::Size(), scale_query, scale_query);
        }
        else
        {
            query.copyTo(resized_query);
        }
    }
    else
    {
        // Assume input is already resized by caller
        query.copyTo(resized_query);
    }

    // Find matches
    // Note: Target and query are flipped
    // Matches are found from target -> query
    auto pts = find_matches(resized_target, resized_query);

    if (pts.first.size() < 4 || pts.second.size() < 4)
    {
        return cv::Mat::zeros(3, 3, CV_64FC1);
    }

    // Scale the points back up
    if (scale_target < 1)
    {
        for (auto &elem : pts.first)
        {
            elem /= scale_target;
        }
    }

    if (scale_query < 1)
    {
        for (auto &elem : pts.second)
        {
            elem /= scale_query;
        }
    }

    // compute the transform
    // Query -> Target

    // Scaled query size
    const auto sw = static_cast<int>(std::round(resized_target.cols / scale_target));
    const auto sh = static_cast<int>(std::round(resized_target.rows / scale_target));
    // const auto sw = static_cast<int>(std::round(resized_query.cols / scale_query));
    // const auto sh = static_cast<int>(std::round(resized_query.rows / scale_query));
    return find_transform(pts.second, pts.first, _transform, cv::Size(sw, sh));
    // return the transform
}

cv::Mat warp_tps(
    const cv::Mat &query,
    const cv::Mat W,
    const cv::Size &target_size)
{
    const auto n_cp = W.rows - 3;
    auto dst = cv::Mat(target_size, query.type(), cv::Scalar(255, 255, 255));
    cv::Mat map_x(target_size, CV_32FC1);
    cv::Mat map_y(target_size, CV_32FC1);

    double rx, ry, r, r2;
    double x, y, xi, yi;
    double x_non_linear_terms, y_non_linear_terms;
    double x_affine_terms, y_affine_terms;

    for (unsigned int j = 0; j < target_size.height; j++)
    {
        for (unsigned int i = 0; i < target_size.width; i++)
        {
            xi = i + 0.5;
            yi = j + 0.5;
            x_non_linear_terms = 0.0;
            y_non_linear_terms = 0.0;
            for (unsigned int k = 0; k < n_cp; ++k)
            {
                // cout << " k=" << k << endl;
                rx = W.at<double>(k, 2) - xi;
                ry = W.at<double>(k, 3) - yi;
                r = sqrt(rx * rx + ry * ry);
                r2 = r * r;
                x_non_linear_terms += W.at<double>(k, 0) * r2 * log(r);
                y_non_linear_terms += W.at<double>(k, 1) * r2 * log(r);
                // cout << "(" << x_non_linear_terms << "," << y_non_linear_terms << ")" << flush;
            }
            x_affine_terms = W.at<double>(n_cp, 0) + W.at<double>(n_cp + 1, 0) * xi + W.at<double>(n_cp + 2, 0) * yi;
            y_affine_terms = W.at<double>(n_cp, 1) + W.at<double>(n_cp + 1, 1) * xi + W.at<double>(n_cp + 2, 1) * yi;
            map_x.at<float>(j, i) = x_affine_terms + x_non_linear_terms;
            map_y.at<float>(j, i) = y_affine_terms + y_non_linear_terms;
        }
    }
    cv::remap(query, dst, map_x, map_y, cv::INTER_CUBIC);
    return dst;
}

cv::Mat transform::warp_image(
    const cv::Mat &query,
    const cv::Mat &H,
    const cv::Size &target_size,
    const std::string &_transform)
{
    cv::Mat out;
    if (_transform == "perspective")
    {
        cv::warpPerspective(
            query,
            out,
            H,
            target_size,
            cv::INTER_LINEAR | cv::WARP_INVERSE_MAP);
    }
    else if (_transform == "tps")
    {
        out = warp_tps(query, H, target_size);
    }
    else
    {
        cv::warpAffine(
            query,
            out,
            H.rowRange(0, 2),
            target_size,
            cv::WARP_INVERSE_MAP | cv::INTER_CUBIC);
    }
    return out;
}

transform::MeanStddev transform::get_luma_mean_stddev(const cv::Mat &im)
{
    // convert to lab
    cv::Mat im_f;
    im.convertTo(im_f, CV_32F, 1.0 / 255.0);

    cv::Mat lab;
    cv::cvtColor(im_f, lab, cv::COLOR_BGR2Lab);

    // Split lab
    std::vector<cv::Mat> channels;
    cv::split(lab, channels);

    // Convert to 32F and scale 0 -> 1
    cv::Mat l;
    channels[0].convertTo(l, CV_32F, 1.0);

    // compute mean and stddev of l
    cv::Scalar _mean, _stddev;
    cv::meanStdDev(l, _mean, _stddev);

    return transform::MeanStddev{_mean[0], _stddev[0]};
}