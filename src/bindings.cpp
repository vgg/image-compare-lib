#include "transform.hpp"
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <emscripten/bind.h>

cv::Mat _getMatFromTypedArray(const emscripten::val &im, const unsigned int r, bool gray = false)
{
    const unsigned int l = im["length"].as<unsigned int>();
    const unsigned int c = l / r / 4;
    auto frame = cv::Mat(r, c, CV_8UC4);
    auto dst = cv::Mat(r, c, gray ? CV_8UC1 : CV_8UC3);
    emscripten::val memoryView{emscripten::typed_memory_view(l, frame.data)};
    memoryView.call<void>("set", im);
    cv::cvtColor(frame, dst, gray ? cv::COLOR_RGBA2GRAY : cv::COLOR_RGBA2BGR);
    return dst;
}

cv::Mat _getDoubleMatFromTypedArray(const emscripten::val &im, const unsigned int r)
{
    const unsigned int l = im["length"].as<unsigned int>();
    const unsigned int c = l / r;
    auto frame = cv::Mat(r, c, CV_64FC1);
    emscripten::val memoryView{emscripten::typed_memory_view(l, (double *)frame.data)};
    memoryView.call<void>("set", im);
    return frame;
}

emscripten::val estimate_transform(
    const emscripten::val &im1,
    const unsigned int r1,
    const emscripten::val &im2,
    const unsigned int r2,
    const std::string _transform,
    const double scale_im1,
    const double scale_im2)
{
    // Convert input into cv mat
    auto f1 = _getMatFromTypedArray(im1, r1);
    auto f2 = _getMatFromTypedArray(im2, r2);

    // Get transformed image
    auto out = transform::estimate_transform(f1, f2, _transform, scale_im1, scale_im2);
    return emscripten::val(emscripten::typed_memory_view(out.total() * out.elemSize(), out.data));
}

emscripten::val warp_image(
    const emscripten::val &query,
    const unsigned int query_rows,
    const emscripten::val &H,
    const unsigned int target_rows,
    const unsigned int target_cols,
    const std::string _transform)
{
    // Convert input into cv mat
    const auto _query = _getMatFromTypedArray(query, query_rows);
    auto _H = _getDoubleMatFromTypedArray(H, 1);
    if (_transform == "tps")
    {
        _H = _H.reshape(1, _H.cols * _H.rows / 4);
    }
    else
    {
        _H = _H.reshape(1, 3);
    }
    const auto _target_size = cv::Size(target_cols, target_rows);

    auto out = transform::warp_image(_query, _H, _target_size, _transform);
    cv::Mat dst;

    cv::cvtColor(out, dst, cv::COLOR_BGR2RGBA);

    return emscripten::val(emscripten::typed_memory_view(dst.total() * dst.elemSize(), dst.data));
}

emscripten::val estimate_transform(
    const emscripten::val &im1,
    const unsigned int r1,
    const emscripten::val &im2,
    const unsigned int r2)
{
    return estimate_transform(im1, r1, im2, r2, "affine", -1.0, -1.0);
}

emscripten::val estimate_transform(
    const emscripten::val &im1,
    const unsigned int r1,
    const emscripten::val &im2,
    const unsigned int r2,
    const std::string _transform)
{
    return estimate_transform(im1, r1, im2, r2, _transform, -1.0, -1.0);
}

emscripten::val invert_transform(
    const emscripten::val &H)
{
    auto f1 = _getDoubleMatFromTypedArray(H, 3);
    cv::Mat out;
    cv::invert(f1, out);
    return emscripten::val(emscripten::typed_memory_view(out.total() * out.elemSize(), out.data));
}

transform::MeanStddev estimate_luma_mean_stddev(
    const emscripten::val &im,
    const unsigned int r)
{
    auto f = _getMatFromTypedArray(im, r);
    return transform::get_luma_mean_stddev(f);
}

EMSCRIPTEN_BINDINGS(my_module)
{
    emscripten::value_array<transform::MeanStddev>("MeanStddev")
        .element(&transform::MeanStddev::mean)
        .element(&transform::MeanStddev::stddev);

    emscripten::function(
        "estimate_luma_mean_stddev",
        &estimate_luma_mean_stddev);

    emscripten::function(
        "invert_transform",
        &invert_transform);

    emscripten::function(
        "warp_image",
        &warp_image);

    emscripten::function(
        "estimate_transform",
        emscripten::select_overload<emscripten::val(
            const emscripten::val &,
            const unsigned int,
            const emscripten::val &,
            const unsigned int)>(&estimate_transform));

    emscripten::function(
        "estimate_transform",
        emscripten::select_overload<emscripten::val(
            const emscripten::val &,
            const unsigned int,
            const emscripten::val &,
            const unsigned int,
            const std::string)>(&estimate_transform));

    emscripten::function(
        "estimate_transform",
        emscripten::select_overload<emscripten::val(
            const emscripten::val &,
            const unsigned int,
            const emscripten::val &,
            const unsigned int,
            const std::string,
            const double,
            const double)>(&estimate_transform));
}