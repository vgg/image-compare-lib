#include <vector>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "transform.hpp"

int main(int argc, char const *argv[])
{
    if (argc < 3)
    {
        std::cout << "Usage: " << argv[0] << " target query (transform)";
        return -1;
    }

    auto target = cv::imread(argv[1], cv::IMREAD_COLOR);
    if (target.empty())
    {
        std::cout << "Image '" << argv[1] << "' empty";
        return -1;
    }

    auto query = cv::imread(argv[2], cv::IMREAD_COLOR);
    if (query.empty())
    {
        std::cout << "Image '" << argv[2] << "' empty";
        return -1;
    }

    std::string transform = "affine";
    if (argc == 4)
    {
        std::string transform_arg(argv[3]);
        if (transform_arg == "identity" || transform_arg == "translation" || transform_arg == "similarity" || transform_arg == "rigid" || transform_arg == "affine" || transform_arg == "perspective" || transform_arg == "tps")
        {
            transform = transform_arg;
        }
    }
    std::cout << "Applying " << transform << " transformation" << std::endl;

    auto H = transform::estimate_transform(query, target, transform);

    // Warp image
    std::cout << H << std::endl;
    const auto out = transform::warp_image(query, H, target.size(), transform);

    cv::Mat target_resized;
    cv::Mat query_resized;

    const auto scale = 1.0 * 1024 / std::max(target.rows, target.cols);
    if (scale < 1)
    {
        cv::resize(target, target_resized, cv::Size(), scale, scale);
    }
    else
    {
        target.copyTo(target_resized);
    }
    const auto scale_query = 1.0 * 1024 / std::max(query.rows, query.cols);
    if (scale_query < 1)
    {
        cv::resize(query, query_resized, cv::Size(), scale_query, scale_query);
    }
    else
    {
        query.copyTo(query_resized);
    }

    auto ms_target = transform::get_luma_mean_stddev(target_resized);
    auto ms_query = transform::get_luma_mean_stddev(query_resized);

    std::cout << ms_target.mean << " " << ms_target.stddev << " " << ms_query.mean << " " << ms_query.stddev << std::endl;
    // Convert query to lab
    cv::Mat out_f;
    out.convertTo(out_f, CV_32F, 1.0 / 255.0);
    cv::Mat out_lab;
    cv::cvtColor(out_f, out_lab, cv::COLOR_BGR2Lab);

    // Split channels
    std::vector<cv::Mat> channels;
    cv::split(out_lab, channels);

    cv::Mat f;
    channels[0].convertTo(f, CV_32F, 1.0);

    f -= ms_query.mean;
    f *= ms_target.stddev / ms_query.stddev;
    f += ms_target.mean;

    f.convertTo(channels[0], CV_32F, 1.0);

    // merge it back
    cv::merge(channels, out_lab);
    // Convert it back to rgb
    cv::cvtColor(out_lab, out_f, cv::COLOR_Lab2BGR);
    out_f.convertTo(out, CV_8U, 255.0);

    // Max dim 1024 resize for showing
    cv::Mat out_resized;
    if (out.rows > 1024 || out.cols > 1024)
    {
        const auto scale = 1.0 * 1024 / std::max(out.rows, out.cols);
        cv::resize(out, out_resized, cv::Size(), scale, scale);
    }
    else
    {
        out.copyTo(out_resized);
    }
    // Show
    bool flag = false;
    while (true)
    {
        cv::imshow("Warped", flag ? out_resized : target_resized);
        flag = !flag;

        auto v = cv::waitKey(500);
        if (v == 27 || v == 'q')
        {
            break;
        }
    }

    return 0;
}
