# Image Compare Library

Library for registration / aligning images. Provides a JS interface (through WASM) [and soon Python - bindings through pybind11].

- Provides Similarity, Affine, Perspective, Thin Plate Spline transforms for alignment
- Uses SIFT features for correspondence calculation.

## Usage
### JS
1. See `demo/index.html`


### Direct as CPP Library
1. See `src/transform.hpp` for the API
2. See `demo/main.cpp` for an illustration of usage

## Installation
### JS
Just grab a copy from releases and use it :)
NPM build - coming soon!

### Python, CPP lib
PyPI - Coming soon!
Currently it is required to be built from source

## Dependencies
- OpenCV 4 (for SIFT, Transforms) - Check [vgg/packages](https://gitlab.com/vgg/packages/-/packages/8351401) registry for a precompiled version, and instructions for how to compile a new OpenCV version as well.
- EIGEN (for linear algebra ops)
- Emscripten (for WASM)
- PyBind11 (for Python bindings)
- WebGL (optional, for performance on web apps)

## Acknowledgements

## Development
Check `docs/dev.md` on how to setup a local development environment to modify the library. Pull Requests welcome!
